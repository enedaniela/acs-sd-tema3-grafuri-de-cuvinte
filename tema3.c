#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include <ctype.h>
#define MAXBUFF 1000
#define MAXCUV 100000
#define MAXL 100
#define MAXT 1000000
#define MAXCUV_T 100000

//Structura pentru bi-grame
typedef struct {
  char *w1;
  char *w2;
  float o;
  float c;
  int nr_w1;
  int nr_w2;
}bigram;

//Structuri pentru graf
typedef struct nod
{
  int indice_in_bigrame;
  struct nod *next;  
}nod;

typedef struct 
{

  int n; // numarul de noduri
  nod *a[MAXCUV];
  
}graf;

//Structura pentru coada de prioritati
struct node
{
    float priority;
    int info;
    struct node *link;
}*front=NULL;

char matstr[MAXCUV_T][MAXL];
//Functii pentru coada cu prioritatie
//--------------------------------------------------------------------------

/*Functie ce verifica daca coada este goala*/
int isEmpty()
{
    if( front == NULL )
      return 1;
    else
      return 0;

}

/*Functie ce insereaza un element in coada de prioritati*/
void insert(int item,float item_priority)
{
    struct node *tmp,*p;

    tmp=(struct node *)malloc(sizeof(struct node));
    if(tmp==NULL)
    {
      printf("Memory not available\n");
      return;
    }
    tmp->info=item;
    tmp->priority=item_priority;
    /*Coada este goala sau a fost intodus un element cu o prioritate mei mica*/
    if( isEmpty() || (item_priority < front->priority) )
    {
      tmp->link=front;
      front=tmp;
    }
    else
    {
      p = front;
      while( p->link!=NULL && p->link->priority<=item_priority )
        p=p->link;
      tmp->link=p->link;
      p->link=tmp;
   }
}

/*Functie ce sterge un element din coada trimis ca parametru*/
void del(int nod)
{
  struct node *tmp;
  struct node *p,*last;
   if(isEmpty()) 
    return;

  last=NULL;
  for(p=front;p!=NULL && p->info!=nod;last=p,p=p->link);

  if(p==NULL)
    return;
  if(p==front)
  {
    tmp=front;
    front=front->link;
    free(tmp);
  }
  else
  {
    last->link=p->link;
    free(p);
  }
           
}

/*Functie ce afiseaza coada*/
void display()
{
    struct node *ptr;
    ptr=front;

    if( isEmpty() )
      printf("Queue is empty\n");
    else
    {    
      printf("Queue is :\n");
      printf("Priority       Item\n");
      while(ptr!=NULL)
      {
        printf("%f        %d\n",ptr->priority,ptr->info);
        ptr=ptr->link;
      }
    }
}
//-------------------------------------------------------------------------

/*Functie ce initializeaza graful*/
void initG (graf *g, int n)
{
  int i;

  g->n = 0;
  for (i = 0; i < n; i++)
    g->a[i] = NULL;
}

/*Functie ce adauga o muchie in graf*/
void addM ( graf *g, bigram x)
{
  nod *start, *end;
  
  start = malloc(sizeof(nod));
  end = malloc(sizeof(nod));

  start->indice_in_bigrame = x.nr_w1;
  start->next = end;
  end->next = NULL;
  end->indice_in_bigrame = x.nr_w2;
  g->a[g->n] = start;
  g->n++;
}

/*Functie ce afiseaza graful*/
void show(graf* g)
{
  int i;
  nod *aux;

  for(i = 0; i < g->n; i++)
  {
    aux = g->a[i];
    if(aux != NULL)
    {
      while(aux->next != NULL)
      {
        printf("%d  ", aux->indice_in_bigrame);
        aux = aux->next;
      }
      printf("%d  ", aux->indice_in_bigrame);
      printf("\n");
    }
  }
}

/*Functie ce returneaza costul corespunzator unei muchii*/
float findCost(bigram * sintag, int t, int u, int v)
{
  int j;

  for(j = 0; j < t; j++)
    if(sintag[j].nr_w1 == u && sintag[j].nr_w2 == v)
      return sintag[j].c;
    return 0;
}
  
/*Functie ce implementeaza algoritmul Dijkstra*/
void Dijkstra(graf* g, int nr_cuv, bigram *sintag, int t, 
  int start, int stop, FILE *fid)
{
  float dist[MAXCUV];
  int prev[MAXCUV], viz[MAXCUV];
  int i;
  float co;
  int u, v;
  
  //initializez vectorul de distante
  for(i = 0; i < nr_cuv; i++)
    dist[i] = 1000000;
  dist[start] = 0;

  //initializez vectorul de predecesori
  for(i = 0; i < nr_cuv; i++)
    prev[i] = -1,viz[i]=0;

  u = start;
  if (g->a[u] == NULL)
  {
    printf("Impossible way\n");
    return;
  }
  insert(u,dist[u]); 
  while(!isEmpty())
  {
    u = front->info;
    viz[u]=1;
    del(u);
  
    for(i = 0; i < t; i++)
      if(sintag[i].nr_w1 == u && viz[ sintag[i].nr_w2 ]==0 ) 
      {
        v = sintag[i].nr_w2;
        co = findCost(sintag,t,u,v);
        if(dist[u] + co < dist[v])
        {
          dist[v] = dist[u] + co;
          prev[v] = u;
          del(v);
          insert(v, dist[v]);
        }
      } 
  }

int indici[MAXCUV];
int nr=0;

  for(i=stop;i!=start;i=prev[i]) 
    indici[++nr]=i;
    indici[++nr]=start;

  for(i=nr;i;i--) 
  {
    fprintf(fid,"%s",matstr[indici[i]]);
    if(i>1) 
      fprintf(fid," ");
  }
  fprintf(fid,"\n");

}

/*Functie ce calculeaza ratia*/
float oddsratio(int o1,int o2, int o3, int o4)
{
  float x;

  x = log(o1 + 0.5) + log(o4 + 0.5) - log(o3 + 0.5) - log(o2 + 0.5);

  return x;
}

/*Functie ce calculeaza costul*/
float cost(float max, float oddrat)
{
  float x;
  x = 1 +  max - oddrat;
  return x;
}
 

int main (int argc,char **argv)
{
  FILE * fid, * fid2, * fid3;
  char str[MAXT];
  char file[MAXBUFF],fileFinal[20];
  char * pch;
  int i = 0, j, k, o1, o2, o3, o4, nr_cuv = 0, t = 0;
  float max = 0;
  char *elim = "!.,?\":();_*£$%^&";
  char aux[MAXT];
  bigram sintag[MAXCUV_T];

/*Deschid fisierul*/
  fid = fopen(argv[1], "r");

/*Citesc numele fisierului din care se va citi textul*/
  fgets(file,MAXBUFF,fid);
  for(i=0;i<strlen(file)-1;i++)
    fileFinal[i] = file[i];
  fileFinal[strlen(file)-1]='\0';

/*Citesc textul*/
  fid2 = fopen(fileFinal,"r");
  fgets(str,MAXBUFF,fid2); 
  while(!feof(fid2))
  {
    aux[0]='\0';
    fgets(aux,MAXBUFF,fid2);
    strcat(str,aux);
  }

/*Elimin caractere */
  for(i = 0; i < strlen(str); i++)
    for(j = 0; j < strlen(elim); j++)
    {
      if(str[i]==elim[j])
      {
      //Elimin caracterul
        memmove(str+i,str+i+1,sizeof(str));
        i--;
      }
    }

/*Impart textul in cuvinte*/
  pch = strtok (str," -\n");
  strcpy(matstr[nr_cuv],pch);
  nr_cuv++;
  while (pch != NULL)
  {
    pch = strtok (NULL, " -\n");
    if(pch != NULL)
    {
      strcpy(matstr[nr_cuv],pch);
      nr_cuv++;
    }
  }

/*Transform caracterele mari in caractere mici*/
  for(j = 0; j < nr_cuv; j++)
  {
    for(k = 0; k < strlen(matstr[j]); k++)
       matstr[j][k] = tolower(matstr[j][k]);
  }
/*Calculez ratiile*/
  for(j = 0; j < nr_cuv - 1; j++)
  {
    o1 = 0;
    o2 = 0;
    o3 = 0;
    o4 = 0;
    
    sintag[t].w1=strdup(matstr[j]); 
    sintag[t].w2=strdup(matstr[j+1]);
   for(k = 0; k < nr_cuv - 1; k++)
    {
      /*Numar de cate ori se gaseste secventa word1 word2 in text*/
      if(strcmp(sintag[t].w1,matstr[k]) == 0 && 
        strcmp(sintag[t].w2,matstr[k+1]) == 0)
        o1++;
      /*Numar de cate ori word1 este urmat de alt cuvant 
      in afara de word2 in text*/
      if(strcmp(sintag[t].w1,matstr[k]) == 0 && 
        strcmp(sintag[t].w2,matstr[k+1]) != 0)
        o2++;
      /*Numar de cate ori word2 este precedat de alt cuvant
       in afara de word1 in text*/
      if(strcmp(sintag[t].w1,matstr[k]) != 0 && 
        strcmp(sintag[t].w2,matstr[k+1]) == 0)
        o3++;
      /*Numar de cate ori nu se gaseste secventa word1 word2
       in text*/
      if(strcmp(sintag[t].w1,matstr[k]) != 0 && 
        strcmp(sintag[t].w2,matstr[k+1]) != 0)
        o4++;
    }
    sintag[t].o = oddsratio(o1,o2,o3,o4);
    t++;
   
  }
  /*Gasesc maximul dintre oddratio*/
  for(j = 0; j < t; j++)
    if(max < sintag[j].o)
      max = sintag[j].o;

  /*Calculez costurile*/
  for(j = 0; j < t; j++)
  {
    sintag[j].c = cost(max,sintag[j].o);
  }

/*Sortez unic cuvintele din matricea de cuvinte pentru a adauga nodurile*/
  for (i = 0; i < nr_cuv; i++) 
  {
      for (j = i + 1; j < nr_cuv;) 
      {
         if (strcmp(matstr[j], matstr[i])==0)
        {
            for (k = j; k < nr_cuv; k++) 
            {
               memcpy(matstr[k],(matstr[k + 1]),sizeof(matstr[k+1]));
            }
            nr_cuv--;
         } else
            j++;
      }
   }

  graf *g = malloc(sizeof(graf));
  initG(g, nr_cuv);

  /*Sortez unic sintagmele word1 word2 pentru a creea muchiile*/
  for (i = 0; i < t; i++) 
  {
        for (j = i + 1; j < t;) 
        {
           if (strcmp(sintag[j].w1, sintag[i].w1) == 0 && 
            strcmp(sintag[j].w2, sintag[i].w2) == 0) 
           {
              for (k = j; k < t; k++) 
              {
                 sintag[k] = sintag[k + 1];
              }
              t--;
           } 
           else
              j++;
        }
  }
  /*Asociez cuvintelor indici*/
  for(j = 0; j < t; j++)
  {
    for(i = 0; i < nr_cuv; i++)
    {
      if(strcmp(sintag[j].w1,matstr[i])==0)
        sintag[j].nr_w1 = i;
      
      if(strcmp(sintag[j].w2,matstr[i])==0)
        sintag[j].nr_w2 = i; 
    }
   
    addM(g,sintag[j]);     
  }

  int nr_linii;
  char *word1, *word2;
  char str_line[MAXL*2];
  fscanf(fid,"%d\n",&nr_linii);

  /*Deschid fisierul pentru scriere*/
  fid3 = fopen(argv[2], "a");
  /*Citesc cuvintele pentru care trebuie sa afisez costul*/
  for(i = 0; i < nr_linii; i++)
  {
    fgets(str_line,MAXL*2,fid);
    word1 = strtok(str_line," ");
    word2 = strtok(NULL,"\n");
    for(j = 0; j < t; j++)
      if(strcmp(sintag[j].w1,word1)==0 && strcmp(sintag[j].w2,word2)==0)
        fprintf(fid3,"%f\n",sintag[j].c);
  }

  int nr_linii_dijk,ind_w1,ind_w2;

  /*Citesc cuvintele pentru care trebuie sa gasesc drumul de cost minim*/
  fscanf(fid,"%d\n",&nr_linii_dijk);
  for(i = 0; i < nr_linii_dijk; i++)
  {
    fgets(str_line,MAXL*2,fid);
    word1 = strtok(str_line," ");
    word2 = strtok(NULL,"\n");
    for(j = 0; j < nr_cuv; j++)
    {
      if(strcmp(word1,matstr[j])==0)
        ind_w1 = j;
      else 
        if(strcmp(word2,matstr[j])==0)
          ind_w2 = j;
    }
    Dijkstra(g,nr_cuv,sintag,t,ind_w1,ind_w2,fid3);
  }

/*Inchide fisierele*/
  fclose(fid);
  fclose(fid2);
  fclose(fid3);
  return 0;
}
